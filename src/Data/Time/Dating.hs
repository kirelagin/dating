module Data.Time.Dating
    ( DateSpan (DateSpan)
    , spanStart
    , spanLen
    , spanEnd

    , trimSpanStart
    , trimSpanEnd

    , inSpan

    , trimSpanList

    , Day
    ) where

import Data.List (dropWhile, takeWhile, sort)
import Data.Time (Day, addDays, diffDays)


data DateSpan = DateSpan Day Integer
  deriving (Eq, Ord)

instance Show DateSpan where
  show s = show (spanStart s) ++ " - " ++ show (spanEnd s)

spanStart :: DateSpan -> Day
spanStart (DateSpan day _) = day

spanLen :: DateSpan -> Integer
spanLen (DateSpan _ len) = len

-- Not in span
spanEnd :: DateSpan -> Day
spanEnd (DateSpan day len) = addDays len day

trimSpanStart :: Day -> DateSpan -> DateSpan
trimSpanStart newStart s@(DateSpan day _)
  | newStart <= day = s
  | otherwise = DateSpan newStart (if newLen > 0 then newLen else 0)
 where
  newLen = diffDays (spanEnd s) newStart

trimSpanEnd :: Day -> DateSpan -> DateSpan
trimSpanEnd newEnd s@(DateSpan day _)
  | newEnd < day = DateSpan day 0
  | newEnd >= spanEnd s = s
  | otherwise = DateSpan day (diffDays newEnd day)

inSpan :: DateSpan -> Day -> Bool
inSpan (DateSpan day len) d = d >= day && diffDays day d < len

-- @to@ is not included
trimSpanList :: Day -> Day -> [DateSpan] -> [DateSpan]
trimSpanList from to
  = map (trimSpanStart from . trimSpanEnd to)
  . takeWhile ((< to) . spanStart)
  . dropWhile ((<= from) . spanEnd)
  . sort
