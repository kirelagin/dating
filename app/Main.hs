{-# LANGUAGE TypeFamilies #-}

module Main where

import Control.Monad (void)
import Data.Time (diffDays)
import Data.Time.Format (parseTimeM, defaultTimeLocale, iso8601DateFormat)
import Data.Void (Void)
import qualified Data.Text.IO as T
import System.Environment (getArgs)
import System.IO (stdin)
import Text.Megaparsec (parse, errorBundlePretty, Parsec, Stream, some, (<|>), many, Token, eof)
import Text.Megaparsec.Char (char, newline, space, space1, digitChar)

import Data.Time.Dating (DateSpan (..), trimSpanList, spanLen)


spanP :: (Stream s, Token s ~ Char) => Parsec Void s DateSpan
spanP = do
  dayStart <- dateP
  void $ space1 *> char '-' *> space1
  dayEnd <- dateP
  let len = diffDays dayEnd dayStart
  if len < 0 then fail "Negative span" else pure $ DateSpan dayStart (len + 1)
 where
  dateP = some (digitChar <|> char '-') >>= parseTimeM False defaultTimeLocale fmt
  fmt = iso8601DateFormat Nothing

spanListP :: (Stream s, Token s ~ Char) => Parsec Void s [DateSpan]
spanListP = many $ space *> char '-' *> space *> spanP <* newline

main :: IO ()
main = do
  [from, to] <- map read <$> getArgs
  input <- T.hGetContents stdin
  case parse (spanListP <* eof) "<stdin>" input of
    Left e -> putStr (errorBundlePretty e)
    Right ss -> print $ process from to ss
 where
  process from to = sum . map spanLen . trimSpanList from to
